package es.cipfpbatoi;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class App {

    private static final String BIBLIOTECA_XML = "biblio-jaxb.xml";

    public static void main(String[] args) {

        ArrayList<Libro> listaLibros1 = new ArrayList<Libro>();

        ArrayList<Biblioteca> bibliotecas = new ArrayList<Biblioteca>();

        // Creamos los libros
        Libro libro1 = new Libro();
        libro1.setId(1);
        libro1.setIsbn("978-0060554736");
        libro1.setTitulo("The Game");
        libro1.setAutor("Neil Strauss");
        libro1.setEditor("Harpercollins");
        libro1.setPaginas((short) 200);
        listaLibros1.add(libro1);

        Libro libro2 = new Libro();
        libro2.setId(2);
        libro2.setIsbn("978-3832180577");
        libro2.setTitulo("Feuchtgebiete");
        libro2.setAutor("Charlotte Roche");
        libro2.setEditor("Dumont Buchverlag");
        libro2.setPaginas((short) 100);
        listaLibros1.add(libro2);
        
        Libro libro3 = new Libro(3, "Odisea Xml", "Moisés Arjona", "Moises Arjona", "123-412343423", (short) 100);
        listaLibros1.add(libro3);

        // Creamos la biblioteca y le asignamos los libros
        Biblioteca biblioteca1 = new Biblioteca();
        biblioteca1.setNombre("Fraport Bookstore");
        biblioteca1.setUbicacion("Frankfurt Airport");
        biblioteca1.setListaLibros(listaLibros1);
        bibliotecas.add(biblioteca1);

        Biblioteca biblioteca3 = new Biblioteca();
        biblioteca3.setNombre("Biblioteca municipal Ibi");
        biblioteca3.setUbicacion("Ibi");
        biblioteca3.setListaLibros(listaLibros1);
        bibliotecas.add(biblioteca3);

        Catalogo catalogo = new Catalogo();
        catalogo.setBiblioteca(bibliotecas);

        try {
            // Creamos el contexto JAXB basado en Biblioteca.class
            JAXBContext context = JAXBContext.newInstance(Catalogo.class);

            // Instanciamos un marshaller o serializador
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");               // valor por defecto

            m.marshal(catalogo, System.out);

            // escritura del fichero XML
            try (FileWriter fw = new FileWriter(BIBLIOTECA_XML)) {
                m.marshal(catalogo, fw);
            } catch (IOException ex) {
                System.err.println("Problemas durante la escritura del fichero..." + ex.getMessage());
            }

            // lectura del fichero XML
            Unmarshaller um = context.createUnmarshaller();
            Biblioteca biblioteca2 = null;
            try {
                biblioteca2 = (Biblioteca) um.unmarshal(new FileReader(BIBLIOTECA_XML));
            } catch (FileNotFoundException ex) {

                System.err.println("Parece que el fichero no existe..." + ex.getMessage());
            }

            if (biblioteca2 != null) {
                ArrayList<Libro> listaLibros2 = biblioteca2.getListaLibros();
                System.out.println("Nombre: " + biblioteca2.getNombre());
                System.out.println("Ubicacion: " + biblioteca2.getUbicacion());
//                for (Libro libro : listaLibros2) {
//                    System.out.println(libro);
//                }
                listaLibros2.forEach(System.out::println);
            }

        } catch (JAXBException ex) {
            System.out.println("Problemas durante la lectura/escritura del archivo xml " + BIBLIOTECA_XML + " " + ex.getMessage());
        }

    }
}


