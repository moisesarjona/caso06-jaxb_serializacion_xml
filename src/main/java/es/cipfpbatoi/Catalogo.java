package es.cipfpbatoi;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

@XmlRootElement(name = "catalogo")
@XmlType(propOrder = "biblioteca")
public class Catalogo {
    private ArrayList<Biblioteca> biblioteca;


    @XmlElementWrapper(name = "bibliotecas")
    @XmlElement(name = "biblioteca")
    public ArrayList<Biblioteca> getBiblioteca() { return biblioteca; }
    public void setBiblioteca(ArrayList<Biblioteca> biblioteca) { this.biblioteca = biblioteca; }
}
