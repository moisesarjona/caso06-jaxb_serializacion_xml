package es.cipfpbatoi;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

//Definición del elemento raíz de nuestro documento
@XmlType(propOrder = {"nombre", "listaLibros", "ubicacion"})
public class Biblioteca {
    
    private String nombre;
    private String ubicacion;    
    private ArrayList<Libro> listaLibros;    

    public Biblioteca() {
    }
    
    @XmlElementWrapper(name = "books")
    @XmlElement(name = "book")
    public ArrayList<Libro> getListaLibros() {
        return listaLibros;
    }
    public void setListaLibros(ArrayList<Libro> bl) {
        this.listaLibros = bl;
    }    

    @XmlElement(name = "name")
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlElement(name = "location")
    public String getUbicacion() {
        return ubicacion;
    }
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
